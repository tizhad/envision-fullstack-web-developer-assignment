import "./StartPage.css";
import { useSelector } from "react-redux";
import { selectFileName, selectResult } from "../store/user/selectors";
import { useEffect, useState } from "react";
import history from "../history";
import { Col, Container, Row } from "react-bootstrap";
import { IconContext } from "react-icons";
import { MdPlayArrow, MdRefresh } from "react-icons/md";

export default function ImageResult() {
  const result = useSelector(selectResult);
  const fileName = useSelector(selectFileName);
  const [prettyResult, setPrettyResult] = useState("");

  useEffect(() => {
    if (result === null) {
      history.push("/");
      return;
    }
    const mergedResult = result.reduce((accumulator, currentValue) => {
      return {
        paragraph: accumulator.paragraph + "\n" + currentValue.paragraph,
      };
    });
    setPrettyResult(mergedResult.paragraph);
  }, [result]);

  return (
    <Container>
      <Row>
        <Col>
          <p>
            Output of <span> {fileName}</span>
          </p>
        </Col>
      </Row>
      <Row>
        <Col>
          <p className="text-area">{prettyResult}</p>
        </Col>
      </Row>
      <Row>
        <IconContext.Provider value={{ color: "white", size: "100%" }}>
          <Col xs={{ span: 4, offset: 2 }}>
            <button className="play-btn">
              <MdPlayArrow color="white" />
            </button>
            <small className="btn-txt">Play/pause</small>
          </Col>
          <Col xs={4}>
            <button className="play-btn">
              <MdRefresh color="white" />
            </button>
            <small className="btn-txt">Restart</small>
          </Col>
        </IconContext.Provider>
      </Row>
    </Container>
  );
}
