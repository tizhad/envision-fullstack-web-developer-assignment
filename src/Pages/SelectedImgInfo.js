import { selectResult } from "../store/user/selectors";

import { useSelector } from "react-redux";
import history from "../history";
import { useEffect } from "react";
import "./SelectedImageInfo.css";
import { Container, Row, Col } from "react-bootstrap";

export default function SelectedImageInfo() {
  const extractResult = useSelector(selectResult);

  useEffect(() => {
    if (extractResult === null) {
      history.push("/");
    }
  }, [extractResult]);

  function onButtonClicked(event) {
    history.push("/result");
  }
  return (
    <Container>
      <Row>
        <Col>
          <div>
            <p className="guid-text">
              Upload an image and get the output of the text.
            </p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <hr className="line" />
        </Col>
      </Row>
      <Row>
        <Col>
          <p className="guid-text-upload"> 1 file added successfully!</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <button className="show-result-btn" onClick={onButtonClicked}>
            Display Output{"   "}{" "}
            <span role="img" aria-label="arrow">
              &#x279C;
            </span>
          </button>
          <p className="guid-noFile"> File uploaded</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <hr className="line" />
        </Col>
      </Row>
    </Container>
  );
}
