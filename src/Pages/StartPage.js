import React from "react";
import "./StartPage.css";
import { useDispatch } from "react-redux";
import { doExtract } from "../store/user/actions";
import { Container, Row, Col } from "react-bootstrap";

export default function SelectedImagePage() {
  const dispatch = useDispatch();

  const onFileSelected = (e) => {
    dispatch(doExtract(e.target.files[0]));
  };

  return (
    <Container>
      <Row>
        <Col>
          <div>
            <p className="guid-text">
              Upload an image and get the output of the text.
            </p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <hr className="line" />
        </Col>
      </Row>
      <Row>
        <Col>
          <p className="guid-text-upload">Upload an image</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <input
            style={{ opacity: 5 }}
            className="custom-file-input"
            type="file"
            name="file"
            accept="image/*"
            onChange={onFileSelected}
          ></input>
          <p className="guid-noFile"> No file selected</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <hr className="line" />
        </Col>
      </Row>
    </Container>
  );
}
