import React from "react";
import { Nav, Navbar } from "react-bootstrap";

export default function NavBar() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="light">
      <Navbar.Brand href="https://www.letsenvision.com/">
        <img
          src="https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2.png"
          loading="lazy"
          width="200"
          sizes="201px"
          srcSet="https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2-p-500.png 500w, https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2-p-800.png 800w, https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2-p-1080.png 1080w, https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2-p-1600.png 1600w, https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2-p-2000.png 2000w, https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2.png 3384w"
          alt="Envision"
          className="image-40"
        />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="https://www.letsenvision.com/">HOME</Nav.Link>
          <Nav.Link href="https://www.letsenvision.com/envision-app">
            APP
          </Nav.Link>
          <Nav.Link href="https://www.letsenvision.com/envision-glasses">
            GLASSES
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
