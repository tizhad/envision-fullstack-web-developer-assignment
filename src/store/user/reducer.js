import { SET_FILENAME, SET_RESULT } from "./actions";

const initialState = {
  result: null,
  fileName: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_RESULT:
      return { ...state, result: action.payload };
    case SET_FILENAME:
      return { ...state, fileName: action.payload };
    default:
      return state;
  }
};

export default reducer;
