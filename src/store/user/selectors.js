export const selectResult = (state) => state.user.result;
export const selectFileName = (state) => state.user.fileName;
