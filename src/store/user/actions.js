import axios from "axios";
import {
  appLoading,
  appDoneLoading,
  showMessageWithTimeout,
  setMessage,
} from "../appState/actions";
import history from "../../history";

export const SET_RESULT = "SET_RESULT";
export const SET_FILENAME = "SET_FILENAME";
const errorMessage = "No text found";

export const doExtract = (selectedFile) => {
  return async (dispatch, getState) => {
    dispatch(appLoading());
    const formData = new FormData();
    formData.append("photo", selectedFile);

    try {
      const response = await axios.post("/api/test/readDocument", formData);
      const data = response.data;
      if (data.hasOwnProperty("message") && data.message === errorMessage) {
        dispatch(showMessageWithTimeout("info", true, data.message));
        dispatch(setResult(null));
        dispatch(setFileName(null));
        dispatch(appDoneLoading());
        history.push("/");
        return;
      }

      dispatch(setFileName(selectedFile.name));
      dispatch(setResult(data.response.paragraphs));
      // dispatch(showMessageWithTimeout("success", true, "image extracted"));
      dispatch(appDoneLoading());
      history.push("/selected-img");
    } catch (error) {
      console.log(error.message);
      dispatch(setMessage("danger", true, error.message));
    }
  };
};

export const setResult = (result) => {
  return {
    type: SET_RESULT,
    payload: result,
  };
};
export const setFileName = (selectedFile) => {
  return {
    type: SET_FILENAME,
    payload: selectedFile,
  };
};

export default doExtract;
