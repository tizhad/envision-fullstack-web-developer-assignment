import React from "react";
import "./App.css";
import NavBar from "./Components/NavBar/NavBar";
import StartPage from "./Pages/StartPage";
import SelectedImgInfo from "./Pages/SelectedImgInfo";
import { Switch, Route } from "react-router-dom";
import ResultPage from "./Pages/ResultPage";
import Loading from "./Components/Loading";
import MessageBox from "./Components/MessageBox";
import { useSelector } from "react-redux";
import { selectAppLoading } from "./store/appState/selectors";

export default function App() {
  const isLoading = useSelector(selectAppLoading);

  return (
    <div>
      <NavBar />
      <MessageBox />
      {isLoading ? <Loading /> : null}
      <Switch>
        <Route exact path="/" component={StartPage} />
        <Route path="/selected-img" component={SelectedImgInfo} />
        <Route path="/result" component={ResultPage} />
      </Switch>
    </div>
  );
}
