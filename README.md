Deployed on Netlify:
https://envision-test.netlify.app/

This project has been written in react js. I used some packages that I've listed below:

- Redux:
  I used redux because I needed to have my app state globally and I wanted to use them in various places.
- Redux thunk: To deal with side effects (e.g. API calls) on my redux actions.
- Using Bootstrap was my selection to develop the front because I can manage front without CSS issues and level out browser differences for various page elements.
- I've used React Router because I needed to push the user from one page to another page, also I wanted to have back and forward buttons in the browser and make a better experience for the user to be able to back to previous or next pages with browser's button instead of writing the URL again and again.
